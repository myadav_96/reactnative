import Video from 'react-native-video';
import React, { Component } from 'react';
import { View,FlatList,StyleSheet,TouchableOpacity,Text,Image } from 'react-native';



export default class VideoList extends Component {
    data = [{ uri:"http://clips.vorwaerts-gmbh.de/VfE_html5.mp4",pause:true,mute:false},
            { uri:"http://clips.vorwaerts-gmbh.de/VfE_html5.mp4",pause:true,mute:false},
            { uri:"http://clips.vorwaerts-gmbh.de/VfE_html5.mp4",pause:true,mute:false},
            { uri:"http://clips.vorwaerts-gmbh.de/VfE_html5.mp4",pause:true,mute:false},]
   
    constructor(props) {
        super(props);
        this.state = {};
      }

    startVideo=()=>{
        this.setState({})
    }  
    
    render() {  
      return (
       <FlatList
        data={this.data}
        extraData={this.state}
        keyExtractor={item => item.uri}
        renderItem={({ item }) => (
            <View style={{padding:10,marginTop:10,borderBottomWidth:1,borderBottomColor:"grey"}}>
                <Video 
                    paused={item.pause}
                    muted={item.mute}
                    source={{uri: item.uri}}   
                    ref={(ref) => { this.player = ref }}        
                    resizeMode="stretch"                              
                    onBuffer={this.onBuffer}                
                    onEnd={this.onEnd}                      
                    onError={this.videoError}               
                    style={styles.backgroundVideo} />
                <TouchableOpacity onPress={()=>{
                    item.pause ? item.pause=false: item.pause=true
                    this.startVideo()}
                }>
                    <Image source={item.pause ? require('./play2.png') : require('./pause2.png')}
                        style={{width: 35, height: 35}}
                    />
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{
                    item.mute ? item.mute=false: item.mute=true
                    this.startVideo()}
                }>
                    <Image source={item.mute ? require('./play2.png') : require('./pause2.png')}
                        style={{width: 35, height: 35}}
                    />
                </TouchableOpacity> 
            </View>
        ) }
        >
       </FlatList>
       );
    }
}

var styles = StyleSheet.create({
  backgroundVideo: {
    height:150,
    width:undefined,
  },
});