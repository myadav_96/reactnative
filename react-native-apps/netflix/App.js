import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import Login from './login';
import VideoList from './videoList';

 class App extends Component {
  render() {
    return (
     <Nav/>
    );
  }
}

const Nav = createStackNavigator({
  Home: { screen: Login,
    navigationOptions: {
      header:null,} },
  videos: { screen: VideoList,
    navigationOptions: {
      header:null,} },
});

export default Nav;