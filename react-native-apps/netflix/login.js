import React, { Component } from 'react';
import { View,TouchableOpacity,Text } from 'react-native';
import { LoginButton, AccessToken } from 'react-native-fbsdk';
import { LoginManager } from 'react-native-fbsdk';
import {VideoList} from './videoList'

export default class Login extends Component {
  render() {
    return (
      <View style={{    flex: 1,
        justifyContent: 'center',
        alignItems: 'center'}}>
        <LoginButton
          onLoginFinished={
            (error, result) => {
              if (error) {
                console.log("login has error: " + result.error);
              } else if (result.isCancelled) {
                console.log("login is cancelled.");
              } else {
                AccessToken.getCurrentAccessToken().then(
                  (data) => {
                    console.log(data.accessToken.toString())
                    this.props.navigation.navigate('videos')
                  }
                )
              }
            }
          }
          onLogoutFinished={() => console.log("logout.")}/>


          <TouchableOpacity onPress={() => this.props.navigation.navigate('videos') }>
            <Text style={{fontSize:20,alignSelf:'center',marginTop:50}}>videos</Text>
          </TouchableOpacity>
      </View>
    );
  }
}

// Attempt a login using the Facebook login dialog asking for default permissions.
// LoginManager.logInWithReadPermissions(['public_profile']).then(
//   function(result) {
//     if (result.isCancelled) {
//       console.log('Login cancelled');
//     } else {
//       console.log('Login success with permissions: '
//         +result.grantedPermissions.toString());
//     }
//   },
//   function(error) {
//     console.log('Login fail with error: ' + error);
//   }
// );