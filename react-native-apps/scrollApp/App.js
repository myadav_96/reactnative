/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import Nav from './src/components/nav.js';

export default class App extends Component {
  render() {
    return (
        <Nav></Nav>
    );
  }
}

const styles = StyleSheet.create({
  
});