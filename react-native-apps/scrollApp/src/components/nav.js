import {createStackNavigator,createBottomTabNavigator} from 'react-navigation';
import {AsyncStorage} from 'react-native';
import React,{Component} from 'react'
import SignIn from './signIn';
import SignUp from './signUp';
import NewsFeed from './newsFeed';
import Profile from './profile';
import {Icon} from 'react-native-elements';
import Settings from './settings';
import UserList from './userList';
  
class TabContainer extends Component {
  navigationOptions= {
    header: null
  }
  render() {
    return (
      <TabBars/>
    );
  }
}

const Nav = createStackNavigator({
  Home: { screen: SignIn },
  signUp: { screen: SignUp },
  // newsFeed:{ screen: NewsFeed },
  // profile: { screen: Profile },
  TabScreen: { screen: TabContainer,
    navigationOptions: {
      header:null,}
  },
});
  
export default Nav;

const TabBars = createBottomTabNavigator({
  newsFeed: {
    screen: UserList,
    navigationOptions: {
      tabBarLabel: "UserList",
      tabBarIcon: ({ tintColor }) => (
        <Icon name='newspaper-o' type='font-awesome' color={tintColor}/>
      )
    }
  },
  // newsFeed: {
  //   screen: NewsFeed,
  //   navigationOptions: {
  //     tabBarLabel: "NewsFeed",
  //     tabBarIcon: ({ tintColor }) => (
  //       <Icon name='newspaper-o' type='font-awesome' color={tintColor}/>
  //     )
  //   }
  // },
  profile: {
    screen: Profile,
    navigationOptions: {
      tabBarLabel: "Profile",
      tabBarIcon: ({ tintColor }) => (
        <Icon name='user' type='font-awesome' color={tintColor}/>
      )
    }
  },
  settings: {
    screen: Settings,
    navigationOptions: {
      tabBarLabel: "settings",
      tabBarIcon: ({ tintColor }) => (
        <Icon name='settings' type='material-icons' color={tintColor}/>
      )
    }
  }
});