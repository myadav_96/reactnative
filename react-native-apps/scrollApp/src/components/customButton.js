import React, {Component} from 'react';
import {StyleSheet,ImageBackground,Text} from 'react-native';

export default class CustomButton extends Component {
  render() {
    return (
        <ImageBackground style={styles.btnImg} source={require('../assets/btn1.png')} >
            <Text style={styles.text}>{this.props.btnTitle}</Text>
        </ImageBackground> 

    );
  }
}

const styles = StyleSheet.create({
    text: {
        color:'#fff',
        fontSize:20,
        marginBottom:15,
        marginTop:15,
        alignSelf:'center'
        
    },
    btnImg:{
        height:60,
        marginTop:30,
        marginLeft:50,
        marginRight:50,

    }
});
