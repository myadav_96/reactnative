import React, {Component} from 'react';
import {StyleSheet,TextInput} from 'react-native';

export default class CustomTextInput extends Component {
  render() {
    return (
        <TextInput
          {...this.props} 
          style={styles.textInput}>
         </TextInput> 
    );
  }
}

const styles = StyleSheet.create({
    textInput :{
        borderColor:"grey",
        borderWidth:1,
        height:60,
        marginLeft:50,
        marginRight:50,
        borderRadius:2,
    }
});
