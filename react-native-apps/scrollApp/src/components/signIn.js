import React, {Component} from 'react';
import {StyleSheet, Text,Keyboard,TouchableWithoutFeedback,TouchableOpacity,Image, View,AsyncStorage} from 'react-native';
import CustomTextInput from './customTextInput';
import CustomButton from './customButton';
import DismissKeyboard from './dismissKeyboard'


export default class SignIn extends Component {
  constructor(props) {
    super(props);
    
    this.checkUserSignedIn();
    this.state = {
      email: '',
      password: '',
    };
  }

  static navigationOptions = {
    title: 'Sign In',
    headerTitleStyle: { 
      textAlign:"center", 
      flex:1 
    },
  };
  

  signIn=(responseJson)=>{

    // const {params} = this.props.navigation.state;
    // if ((params.email===this.state.email) && (params.password===this.state.password))

    if(responseJson.success)
    { 
      Keyboard.dismiss()
      this.addToken(responseJson.token)
    this.props.navigation.navigate('TabScreen')
    }
    else{
      alert("user-not-found.\nPlease Sign Up first.")
    }
  }

  loginUser=async()=> {
    try {
      let response = await fetch('http://192.168.12.39:7000/api/v1/user/authenticateUser',
       {
        method: 'POST',
        headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
       },
        body: JSON.stringify({
          email: this.state.email,
          password:this.state.password,
          }),
        });
      let responseJson = await response.json();
      return this.signIn(responseJson)
      } catch (error) {
      console.error(error);
      }
    }
    
  addToken = async (token) => {
    try {
        await AsyncStorage.setItem('token',token);
        
    } catch (error) {
        alert('Error', 'There was an error.')
    }
  }

  checkUserSignedIn=async ()=>{
    try {
       let value = await AsyncStorage.getItem('token');
       if (value != null){
        this.props.navigation.navigate('TabScreen')
       }
    } catch (error) {
       console.log(error)
    }
}

  render() {
    return (
      <DismissKeyboard>
      <View style={styles.view}>
        <View>
          <Image style={styles.signInImg} source={require('../assets/reactLogo.png')}/> 
          <CustomTextInput 
            placeholder=' E-mail'
            onChangeText={(email) => this.setState({ email })}>
          </CustomTextInput>
          <CustomTextInput 
            placeholder=' Password'
            secureTextEntry={true}
            onChangeText={(password) => this.setState({ password })}>
          </CustomTextInput>
          <TouchableOpacity onPress={this.loginUser}>
          <CustomButton btnTitle='Sign In'></CustomButton>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>alert('Technical error.\nContact developer.')}>
            <Text style={{alignSelf:'center',marginTop:5}}>Forgot your details?</Text>
            </TouchableOpacity>
        </View>  
        <View>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('signUp') }>
            <Text style={{fontSize:20,alignSelf:'center'}}>Create a new account</Text>
          </TouchableOpacity>
        </View>
      </View>
      </DismissKeyboard>
    );
  }
}

const styles = StyleSheet.create({
    signInImg : {
        height:100,
        width:100,
        borderRadius:50,
        alignSelf:'center',
        marginBottom:30
    },
    view : {
      flex:1,
      backgroundColor:'#fff',
      paddingTop:80,
      paddingBottom:10,
      justifyContent:'space-between'
    }
});
