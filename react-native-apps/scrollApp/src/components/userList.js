import React from 'react';
import { FlatList, ActivityIndicator,StyleSheet,AsyncStorage, Text, View,Image  } from 'react-native';

export default class UserList extends React.Component {

  constructor(props){
    super(props);
    this.state ={ isLoading: true,pageNo:1,dataSource:[]}
  }

  componentWillMount(){
    this.getToken()
  }

 
  apiFetch=(token)=>{
    return fetch('http://192.168.12.39:7000/api/v1/user/getUserList/0/'+this.state.pageNo+'/10/', {
        method: 'GET',
        headers: {
          'x-access-token':token,
        // 'x-access-token':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1heWFuay55YWRhdkBrZWxsdG9udGVjaC5jb20iLCJwYXNzd29yZCI6IiQyYSQxMCQ1dGoxMnNQQ3RpMGdPZkNWQnpzMHQuWng2NUxleXBVdWZqN2hDWWJlWTJBWWJrMzVGVG9TSyIsImlhdCI6MTUzNTYxNjQwNSwiZXhwIjoxNTM1NzAyODA1fQ.KZHm-ov9YJSvXcPVOEBIWi3aY76e4TDr3N2RS6LcVIA',
        
        }
      })
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: this.state.dataSource.concat(responseJson.message.results),
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }

  getToken = async () => {
    try {
 //     alert('hey')
        const storedValue = await AsyncStorage.getItem('token');
        this.apiFetch(storedValue)
        this.setState({ pageNo: ++this.state.pageNo });
    //    alert("avc"+storedValue)
    } catch (error) {
        alert('Error', 'There was an error.')
    }
}

  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View style={{flex: 1}}>
      <Text style={{textAlign:'center',backgroundColor:'#0f5fdd',color:'#fff',fontSize:25,padding:10}}>User List</Text>
        <FlatList
          data={this.state.dataSource}
          keyExtractor={(item) => item.email}
          onEndReached={this.getToken}
          onEndReachedThreshold={.7}
          renderItem={({item}) => (
                <View style={{marginTop:5,backgroundColor:'#fff',padding:10,borderBottomColor:'grey',flexDirection:'row'}}>
                    <Image style={{height:50,width:50,borderRadius:50,marginRight:10}} source={{uri:item.picture.medium}}></Image>
                    <View>
                      <Text style={styles.name}>{item.name.title} {item.name.first} {item.name.last}</Text>
                      <Text style={styles.email}>{item.email}</Text>
                    </View>
                </View>          
          )
          }
          
        />
      </View>
    );
  }
}


const styles = StyleSheet.create({
    name:{
        fontSize:20,
    },
    email:{
        fontSize:15,
        textAlign:'left',
    }
    
  });
  