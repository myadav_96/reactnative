import React, {Component} from 'react';
import {StyleSheet,TouchableOpacity,Keyboard, View} from 'react-native';
import CustomTextInput from './customTextInput';
import CustomButton from './customButton';
import DismissKeyboard from './dismissKeyboard'

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      name:'',
      email: '',
      password: '',
    };
  }

  static navigationOptions = {
    title: 'Sign Up',
    headerTitleStyle: { 
      textAlign:"center", 
      flex:1 
    },
  };

  signUp=()=>{
    const regExEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    const regExPass = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/
    const regExName = /^([A-Za-z\s]{3,})$/
    if (regExEmail.test(this.state.email) && regExPass.test(this.state.password) && regExName.test(this.state.name))
    { 
      alert('Sign Up successful.\nPlease login to continue.');
      Keyboard.dismiss()
      this.props.navigation.navigate('Home')
    }
    else{
      alert("invalid name, email or password")
    }
  }

   createUser=async()=> {
    try {
      let response = await fetch('http://192.168.12.39:7000/api/v1/user/createUser',
       {
        method: 'POST',
        headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
       },
        body: JSON.stringify({
          name: this.state.name,
          email: this.state.email,
          password:this.state.password,
          }),
        });
      let responseJson = await response.json();
      return this.signUp
      } catch (error) {
      console.error(error);
      }
    }
  
  render() {
    return (
      <DismissKeyboard>
      <View style={styles.view}>
        <View>
          <CustomTextInput 
            placeholder=' Name'
            onChangeText={(name) => this.setState({ name })}>
          </CustomTextInput>
          <CustomTextInput 
            placeholder=' E-mail'
            onChangeText={(email) => this.setState({ email })}>
          </CustomTextInput>
          <CustomTextInput 
            placeholder=' Password'
            onChangeText={(password) => this.setState({ password })}>    
         </CustomTextInput>
        </View>
        <TouchableOpacity onPress={this.createUser} >
          <CustomButton btnTitle='Sign Up'></CustomButton>
        </TouchableOpacity>
      </View>
      </DismissKeyboard>
    );
  }
}

const styles = StyleSheet.create({
  view : {
    flex:1,
    backgroundColor:'#fff',
    paddingTop:150,
    paddingBottom:200,
    justifyContent:'space-between'
  }
});
