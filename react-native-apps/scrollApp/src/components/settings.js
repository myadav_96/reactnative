import React, {Component} from 'react';
import {StyleSheet,Switch,Slider,View} from 'react-native';
import { Text } from 'react-native-elements';
import {CheckBox} from 'react-native-elements';

export default class Settings extends Component {
 
    state = {
      isSwitchOn1: false,
      isSwitchOn2:false,
      check1:false,
      check2:false,
    }
  
  static navigationOptions = {
    title: 'Settings',
    headerTitleStyle: { 
      textAlign:"center", 
      flex:1 
    },
  };
  
  switchValue1 = () => {
    if (this.state.isSwitchOn1) {
       this.setState({isSwitchOn1:false})
    } else {
      this.setState({isSwitchOn1:true})
      this.setState({isSwitchOn2:false})
    }
  }
  
  switchValue2 = () => {
    if (this.state.isSwitchOn2) {
       this.setState({isSwitchOn2:false})
    } else {
      this.setState({isSwitchOn2:true})
      this.setState({isSwitchOn1:false})
    }
  }

  render() {
    return (
      <View style={{backgroundColor:'#fff',flex:1}}>
        <Text style={{textAlign:'center',backgroundColor:'#0f5fdd',color:'#fff',fontSize:25,padding:10}}>Settings</Text>
       
        <View style={{flexDirection:'row',justifyContent:'space-between',borderBottomWidth:1,borderBottomColor:'#e1e3e8',paddingLeft:20,paddingRight:20,padding:10}}>
        <Text style={{fontSize:20}}>Setting 1</Text>
        <Switch
          onValueChange={this.switchValue1}
          value={this.state.isSwitchOn1} 
          tintColor='#e1e3e8'
          onTintColor='#4286f4'
          thumbTintColor='#4286f4'
        />
        </View>
        <View style={{flexDirection:'row',justifyContent:'space-between',padding:10,borderBottomWidth:1,borderBottomColor:'#e1e3e8',paddingLeft:20,paddingRight:20}}>
        <Text style={{fontSize:20}}>Setting 2</Text>
        <Switch
          value={this.state.isSwitchOn2}
          onValueChange={this.switchValue2}
          tintColor='#e1e3e8'
          onTintColor='#4286f4'
          thumbTintColor='#4286f4'
        />
        </View>
        <View style={{backgroundColor:'#e1e3e8',paddingLeft:20,padding:5}}>
          <Text style={{fontSize:20,fontWeight:'400'}}>Notifications</Text>
        </View>
        <View style={{paddingLeft:20,padding:5}}>
        <CheckBox
            style={{borderBottomWidth:1,borderBottomColor:'#e1e3e8'}}
            title='Account info'
            checkedIcon='dot-circle-o'
            uncheckedIcon='circle-o'
            checked={this.state.check1}
            onPress={() =>this.setState({check1: !this.state.check1})}
          />
        <CheckBox
            style={{borderBottomWidth:1,borderBottomColor:'#e1e3e8'}}
            title='Newsletter'
            checkedIcon='dot-circle-o'
            uncheckedIcon='circle-o'
            checked={this.state.check2}
            onPress={() =>this.setState({check2: !this.state.check2})}
          />
        </View>
        <View style={{paddingLeft:20,backgroundColor:'#e1e3e8',padding:5}}>
          <Text style={{fontSize:20,fontWeight:'400'}}>Brightness</Text>
        </View>
        <View>  
            <Slider
              style={{marginLeft:20,marginRight:20,marginTop:20}}
              minimumTrackTintColor='#4286f4'
              minimumValue={0}
              thumbTintColor='#e1e3e8'
              >

            </Slider>
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
    
});
