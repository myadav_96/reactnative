import React, {Component} from 'react';
import {StyleSheet,TouchableOpacity,View,Image,Text,FlatList} from 'react-native';
import {Icon} from 'react-native-elements';

export default class NewsFeed extends Component {
  newsFeedData = require('../../newsFeedData.json');
  // static navigationOptions = {
  //   title: 'News Feed',
  //   headerTitleStyle: { 
  //     textAlign:"center", 
  //     flex:1 
  //   },
  // };
  
  render() {
    return (
      <View >
        <FlatList
          data={this.newsFeedData}
          keyExtractor={item => item.userEmail}
          renderItem={({ item }) => (
            <View style={styles.userData}>
              <View style={{padding:10,flexDirection:'row',justifyContent:'space-between'}}>
                <TouchableOpacity style={{flexDirection:'row'}} onPress={()=>alert('user details not-found')}>
                  <Image style={styles.userImg} 
                    source={{uri: item.userImg}} />
                  <Text>{item.userName}{"\n"}
                    <Text style={styles.userInfo}>{item.userEmail}</Text>
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flexDirection:'row'}} onPress={()=>alert('unknown location')}>
                  <Text>{item.time}{"\n"}
                    <Text style={styles.userInfo}>in {item.location}</Text>
                  </Text>
                  <Icon name='location-on' type='material-icons'/>
                </TouchableOpacity>
              </View>
              <TouchableOpacity onPress={()=>alert('page not-found')}>
                <Image style={{height:200,width:undefined}} source={{uri: item.image }} />
                <Text style={{margin:20}}>{item.title}</Text>
              </TouchableOpacity>
            </View>
            )}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  userData:{
    marginTop:10,
    marginBottom:10,
    borderBottomWidth:1,
    borderColor:"grey" 
  },
  userImg:{
    height:40,
    width:40,
    borderRadius:50,
    marginRight:10
  },
  userInfo:{
    color:"#e85943",
    fontSize:10
  }
  
});
