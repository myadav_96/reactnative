import React, {Component} from 'react';
import {StyleSheet,TouchableOpacity,Image,Text,View,AsyncStorage} from 'react-native';
import CustomTextInput from './customTextInput';
import CustomButton from './customButton';
import DismissKeyboard from './dismissKeyboard'

export default class Profile extends Component {
  // static navigationOptions = {
  //   title: 'Profile',
  //   headerTitleStyle: { 
  //     textAlign:"center", 
  //     flex:1 
  //   },
  // };

  logout = async() => {
    try {
        await AsyncStorage.removeItem('token');
        // this.props.navigation.navigate('Home');
        //this.props.navigator.push({screen:'Home'})
        
    } catch (error) {
        alert('Error', 'There was an error.')
    }
  }

  imgChange=async()=> {
    try {
      let response = await fetch('http://192.168.12.39:7000/api/v1/user/authenticateUser',
       {
        method: 'POST',
        headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
       },
        body: JSON.stringify({
          email: this.state.email,
          password:this.state.password,
          }),
        });
      let responseJson = await response.json();
      return this.signIn(responseJson)
      } catch (error) {
      console.error(error);
      }
    }

  render() {
    return (
      <DismissKeyboard>
      <View style={styles.view}>
        <View style={{backgroundColor:'#0f5fdd',padding:10,flexDirection:'row',justifyContent:'space-between'}}>
          <Text>&emsp;&emsp;&emsp;</Text>
          <Text style={{fontSize:25,color:'#fff',borderRadius:10}}>Profile</Text>
          <Text 
            style={{fontSize:20,padding:5,backgroundColor:'#fff',borderRadius:10}}
            onPress={()=>this.logout()}>
              SignOut</Text>
        </View>
        <View>
          <Image style={styles.signInImg} source={require('../assets/deku.png')}/> 
          <CustomTextInput value=' Mayank Yadav'></CustomTextInput>
          <CustomTextInput value=' mayank.yadav@kelltontech.com'></CustomTextInput>
          <CustomTextInput value=' +91 9953741894'></CustomTextInput>
        </View>
        <View>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('newsFeed') }>
          <CustomButton btnTitle='Save'></CustomButton>
          </TouchableOpacity>
        </View>    
      </View>
      </DismissKeyboard>
    );
  }
}



const styles = StyleSheet.create({
    signInImg : {
        height:100,
        width:100,
        borderRadius:50,
        alignSelf:'center',
        marginBottom:30
    },
    view : {
      flex:1,
      backgroundColor:'#fff',
      paddingBottom:100,
      justifyContent:'space-between'
    }
});
