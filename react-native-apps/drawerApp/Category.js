import React, {Component} from 'react';
import { View,Text,YellowBox,StyleSheet,FlatList } from 'react-native';

export default class Category extends Component {
    
    constructor(props) {
      super(props);
      YellowBox.ignoreWarnings([
       'Warning: componentWillMount is deprecated',
       'Warning: componentWillReceiveProps is deprecated',
     ],
     state={
        data : '',
     }
    );
    
    }
    
    data = require('./data.json');
   
     render()
     {
        
        return(
           <View 
            style = { styles.MainContainer }
           >
              {/* <Text style={{fontSize: 23}}> Category </Text> */}
              <FlatList
                data={this.data}
                keyExtractor={item => item.id}
                renderItem={({ item }) => (
                <Text style={styles.text} 
                    onPress={()=>{
                     if(item.hasOwnProperty('subCategory')){
                    this.data=item.subCategory 
                    this.setState({data:'new'})
                    }else{
                        alert('no data-found')
                    } }}>
                    {item.name}</Text>
                )}
              />
           </View>
        );
     }
  }
  const styles = StyleSheet.create({
    MainContainer :{
     flex:1,
     marginTop:10,
     },
     text:{
        paddingTop:10,
        paddingLeft:10,
        fontSize:20,
        borderBottomWidth:1,

     }
   });