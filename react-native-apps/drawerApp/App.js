/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, View,Image,Text,TouchableOpacity,DrawerLayoutAndroid} from 'react-native';
import { DrawerNavigator } from 'react-navigation';
import { StackNavigator } from 'react-navigation';
import Category from './Category';
import MyAddress from './myAddress';
import MyOrders from './myOrders';


class App extends Component {

  toggleDrawer=()=>{

    console.log(this.props.navigationProps);
    
    this.props.navigationProps.toggleDrawer();

  }
 
  render() {
    var drawer=(
      <View>
        <View style={{backgroundColor:'#5b90e5',flexDirection:'row',justifyContent:'space-between',padding:10,borderBottomWidth:1}}>  
          <Image
            style={{width: 50, height: 50,borderRadius:50}}
            source={require('./reactLogo.png')}>
          </Image>
          <Text style={{marginTop:12,fontSize:18,color:'#000'}}>Mayank Yadav</Text>
        </View>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Category')}>
        <Text style={styles.textStyle}>Categories</Text>
        </TouchableOpacity>  
        <Text style={styles.textStyle}>My Orders</Text>
        <Text style={styles.textStyle}>My Address</Text>
      </View>);
    return (
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity onPress={this.toggleDrawer} >
          <Image
            source={require('./menu_icon.png')}
            style={{ width: 25, height: 25, marginLeft: 5}}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

class Custom_Side_Menu extends Component {
 
  render() {
 
    return (
      <View>
        <View style={{backgroundColor:'#5b90e5',flexDirection:'row',justifyContent:'space-between',padding:10}}>  
          <Image
            style={{width: 80, height: 80,borderRadius:50}}
            source={require('./reactLogo.png')}>
          </Image>
          <Text style={{marginTop:20,fontSize:25,color:'#000'}}>Mayank Yadav</Text>
        </View>
        <Text style={{fontSize:20,paddingLeft:10,borderBottomWidth:1}} onPress={() => this.props.navigation.navigate('Category')}>Categories</Text> 
        <Text style={{fontSize:20,paddingLeft:10,borderBottomWidth:1}} onPress={() => this.props.navigation.navigate('MyOrders')}>My Orders</Text>
        <Text style={{fontSize:20,padding:5,paddingLeft:10,borderBottomWidth:1}} onPress={() => this.props.navigation.navigate('MyAddress')}>My Address</Text>
      </View>
      
    );
  }
}
    const Category_StackNavigator = StackNavigator({
      Category: { 
        screen: Category, 
        navigationOptions: ({ navigation }) => ({
          title: 'Category',
          headerLeft : <App navigationProps={ navigation }/>,

          headerStyle: {
            backgroundColor: '#4280f4'
          },
          headerTintColor: '#fff',
        })
      },
    });


    const MyAddress_StackNavigator = StackNavigator({
      MyAddress: { 
        screen: MyAddress, 
        navigationOptions: ({ navigation }) => ({
          title: 'My Address',
          headerLeft : <App navigationProps={ navigation }/>,

          headerStyle: {
            backgroundColor: '#4280f4'
          },
          headerTintColor: '#fff',
        })
      },
    });


    const MyOrders_StackNavigator = StackNavigator({
      MyOrders: { 
        screen: MyOrders, 
        navigationOptions: ({ navigation }) => ({
          title: 'My Orders',
          headerLeft : <App navigationProps={ navigation }/>,

          headerStyle: {
            backgroundColor: '#4280f4'
          },
          headerTintColor: '#fff',
        })
      },
    });
    
export default MyDrawerNavigator = DrawerNavigator({
  Category: { 
    screen: Category_StackNavigator
  },
  MyAddress: { 
    screen: MyAddress_StackNavigator
  },
  MyOrders: { 
    screen: MyOrders_StackNavigator
  }},
  {
    contentComponent: Custom_Side_Menu,
});

    
const styles = StyleSheet.create({
  MainContainer :{
   flex:1,
   alignItems: 'center',
   justifyContent: 'center',
   }
 });