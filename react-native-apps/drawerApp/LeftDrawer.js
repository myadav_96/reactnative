import React, {Component} from 'react';
import {StyleSheet,TouchableOpacity, Text, View,DrawerLayoutAndroid,Image} from 'react-native';
import MyOrders from './myOrders';
import Category from './Category';
import { createDrawerNavigator} from 'react-navigation'

export default class LeftDrawer extends Component {
    
        
    static navigationOptions = ({ navigation }) => ({
        title: 'Drawer App',
        headerTitleStyle: {
            alignSelf: 'center',
            fontSize: 20,
            color: 'white',
            flex:1
        },
        headerStyle: {
            height: 55,
            backgroundColor: '#4280f4',
        },
        headerLeft: (
            <TouchableOpacity onPress={()=>{
                 if(_this.drawerOpen){
                     _this.closeDrawer()
                 }else
                     _this.openDrawer()
                 }
                 }>
            <View>
            <Image
                style={{width: 30, height: 30,marginLeft:10}}
                source={require('./menu_icon.png')}
                />
            </View>
        </TouchableOpacity>)
    });

    _this;
    drawer=false;

    componentDidMount(){
        _this=this;
        
    }

    openDrawer=()=>{
        this.DrawerLayoutRef.openDrawer();
        this.drawerOpen= !this.drawerOpen

    }

    closeDrawer=()=>{
        this.DrawerLayoutRef.closeDrawer();
        this.drawerOpen= !this.drawerOpen
    }

  render() {
    var drawer=(
        <View>
          <View style={{backgroundColor:'#5b90e5',flexDirection:'row',justifyContent:'space-between',padding:10,borderBottomWidth:1}}>  
            <Image
              style={{width: 50, height: 50,borderRadius:50}}
              source={require('./reactLogo.png')}>
            </Image>
            <Text style={{marginTop:12,fontSize:18,color:'#000'}}>Mayank Yadav</Text>
          </View>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Category')}>
          <Text style={styles.textStyle}>Categories</Text>
          </TouchableOpacity>  
          <Text style={styles.textStyle}>My Orders</Text>
          <Text style={styles.textStyle}>My Address</Text>
        </View>
    )
    return (
        <DrawerLayoutAndroid
              ref={ref => (this.DrawerLayoutRef = ref)}
              drawerWidth={200}
              drawerPosition={DrawerLayoutAndroid.positions.Left}
              renderNavigationView={() => drawer}>

            <View style={{flex: 1}}>
                <DrawerNavigation/>
            </View>
        </DrawerLayoutAndroid>
      
    );
  }
}

const styles = StyleSheet.create({
    textStyle:{
        padding:10,
        fontSize:14,
        color:'#4a4a4a',
        borderBottomWidth:1,
        borderBottomColor:'#ccc'
      },
  });


  const DrawerNavigation = createDrawerNavigator({
    myOrders: {
      screen: MyOrders,
    },
    Category: {
      screen: Category,
    },
  });