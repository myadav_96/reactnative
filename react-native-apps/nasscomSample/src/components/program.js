import React, {Component} from 'react';
import {TouchableOpacity,Animated,StyleSheet,Text,View,Image,ScrollView,Dimensions} from 'react-native';
//import { Container, Header, Content,TabHeading, Tab, Tabs } from 'native-base';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const scheduleInfo = 'NATC curtain raiser with adobe global CIO, Cynthia (cindy) stoddard "customer & Employee experiance journey"';

const schedule = '../assets/Images.xcassets/ProgramScreen/scheduleIcon.imageset/schedule_nonsel.png'
const scheduleSel = '../assets/Images.xcassets/ProgramScreen/scheduleIcon_sel.imageset/schedule_sel.png'
const myAgenda = '../assets/Images.xcassets/ProgramScreen/agendaIcon.imageset/my_agenda_nonsel.png'
const myAgendaSel = '../assets/Images.xcassets/ProgramScreen/agendaIcon_sel.imageset/my_agenda_sel.png'

export default class Program extends Component {

  static navigationOptions = () => ({
    title: 'NATC 2018',
    headerTintColor: '#fff',
    headerStyle: {
      backgroundColor: '#0f234f',
    },
  });

  state ={
    xOffset : 0,
    scheduleIcon:require(scheduleSel),
    myAgendaIcon:require(myAgenda),
  }
  tabActiveImage=()=>{
    this.state.xOffset>screenWidth/3 ? this.setState({scheduleIcon:require(schedule)}) : this.setState({myAgendaIcon:require(myAgenda)})
    this.state.xOffset<screenWidth/3 ? this.setState({scheduleIcon:require(scheduleSel)}) : this.setState({myAgendaIcon:require(myAgendaSel)})
  }
  scrollAnimation=()=>{
    this.state.xOffset<=screenWidth/3 ? this.scrollView.scrollTo({x:0,animated:true}) : this.scrollView.scrollTo({x:screenWidth,animated:true})
    console.warn("end")
  }
  render() {
    return (
      <View>
          <View style={styles.header}>
              <TouchableOpacity 
                style = {styles.tabNav}
                onPress={() => {this.scrollView.scrollTo({x:0,animated:true})}}>
                <Image source={this.state.scheduleIcon} />
                <Text style={styles.text}>Schedule</Text>
              </TouchableOpacity>
              <TouchableOpacity 
                style = {styles.tabNav}
                onPress={() => {this.scrollView.scrollToEnd()}}>
                <Image source={this.state.myAgendaIcon} />
                <Text style={styles.text}>My Agenda</Text>
              </TouchableOpacity>
          </View>
          <View style={[styles.scrollBar,{left:this.state.xOffset,}]}></View>
          <ScrollView
            horizontal={true}
            pagingEnabled={true}
            scrollEventThrottle={10}
            showsHorizontalScrollIndicator={false}
            ref={scrollView => this.scrollView = scrollView}
            onScroll={ event => { 
              this.setState({xOffset : event.nativeEvent.contentOffset.x*0.5})
              this.tabActiveImage()   }      
            }
            onTouchEnd={event => {
              this.scrollAnimation()
            }}              
          >
            <View style={{width:screenWidth,flex:1}}>
            <View style={{backgroundColor:'grey',height:100,marginBottom:"5%",width:screenWidth}}>
                  
                </View>
              <TouchableOpacity style={{borderBottomWidth:0.5,paddingBottom:"5%",borderBottomColor:'grey'}}>
                <View style={{marginLeft:"2%"}}>
                  <View style={{flexDirection:'row'}}>
                    <Text style={{fontSize:15,color:"#000",width:"90%"}}>{scheduleInfo}</Text>
                    <Image
                    style={{marginTop:"15%"}}
                    source={require('../assets/Images.xcassets/right_arrow.imageset/arrow.png')} />
                  </View>  
                  <Text style={{marginTop:"5%"}}>
                    <Image
                    source={require('../assets/Images.xcassets/ProgramScreen/clock.imageset/time.png')} />
                    time</Text>
                  <Text>
                    <Image
                    source={require('../assets/Images.xcassets/location.imageset/location.png')} />
                    location</Text>
                </View>  
              </TouchableOpacity>  
            </View>
            <View style={{width:screenWidth,height:screenHeight}}>
              <View style={{backgroundColor:'grey',height:100,marginBottom:"5%",width:screenWidth}}>
              </View>
              <TouchableOpacity style={{borderBottomWidth:0.5,padding:"2%",paddingBottom:"10%",borderBottomColor:'grey'}}>
                <Text style={{fontSize:15,color:"#000"}}>No event added!!</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
header : {
    flexDirection:"row",
    justifyContent:"space-around",
},
text : {
    fontSize:18,
    color:"#0f234f",
    marginLeft:"5%",
},
tabNav:{
  flexDirection:"row",
  padding:"3%",
  paddingLeft:"15%",
  paddingRight:"15%",
},
scrollBar:{
  
    height:3,
    width:screenWidth/2,
    backgroundColor:"#0f234f",
  }
});