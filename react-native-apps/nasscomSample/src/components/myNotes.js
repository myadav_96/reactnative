import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,FlatList,Image,TouchableOpacity} from 'react-native';
// import { observer,inject} from 'mobx-react';

 @inject('store')
 @observer
export default class MyNotes extends Component {
  
  static navigationOptions = () => ({
    title: 'My Notes',  
    headerTintColor: '#fff',
    headerStyle: {
        backgroundColor: '#0f234f',
    },
  });  
  
  notes =[
    { event:"Inaugural Act",
      time1:"09:15 AM",
      time2:"09:30 AM",
      date:"Jul 26, 2018",
    },
    { event:"Inaugural Act",
      time1:"09:15 AM",
      time2:"09:30 AM",
      date:"Jul 26, 2018",
    },
    { event:"Inaugural Act",
      time1:"09:15 AM",
      time2:"09:30 AM",
      date:"Jul 26, 2018",
    },
    { event:"Inaugural Act",
      time1:"09:15 AM",
      time2:"09:30 AM",
      date:"Jul 26, 2018",
    },
]

  render() {
    return (
      <FlatList
        style={{backgroundColor:"#fff"}}
        data={this.notes}
        renderItem={({item}) => (
          <TouchableOpacity 
            // onPress={()=>this.props.store.testStore()}
            style={{borderBottomColor:'#000',borderBottomWidth:0.5,padding:10}} >
              <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                <View>
                  <Text style={{color:'#000'}}>{item.event}</Text>
                  <Text>{item.time1}-{item.time2} | {item.date}</Text>
                </View>
                <View>
                  <Image
                    style={{marginTop:20}}
                    source={require('../assets/Images.xcassets/right_arrow.imageset/arrow.png')} />
                </View>
              </View>
                <TouchableOpacity onPress={()=>alert("delete?")} style={{alignSelf:'flex-start'}}>
                  <Text style={{color:'red'}}>Delete</Text>
                </TouchableOpacity>
          </TouchableOpacity>
        )}
      >
      </FlatList>
    );
  }
}

const styles = StyleSheet.create({
});
