import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import Program from './src/components/program';
import { createStackNavigator } from 'react-navigation';
import MyNotes from './src/components/myNotes';
import { Provider } from 'mobx-react'
import Store from './src/store/store';

const store = new Store();

const stores = {
store
}
export default class App extends Component {
  render() {
    return (
      <Provider {...stores}>
      <RootStack  />
    </Provider>
    );
  }
}

const RootStack = createStackNavigator({
  // program: {
  //   screen: Program
  // },
  myNotes:{
    screen: MyNotes
  }
});