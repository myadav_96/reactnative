/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TouchableOpacity} from 'react-native';
import Store from './store';
import { observer,inject} from 'mobx-react';

const store = new Store();

const stores = {
store
}

@inject('store')
@observer
export default class App extends Component {
  render() {
    return (
      <Provider {...stores}>
      <TouchableOpacity 
        onPress={()=>this.props.store.testStore()}>

      </TouchableOpacity>
    </Provider>
    );
  }
}

const styles = StyleSheet.create({
  
});
