import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,ScrollView} from 'react-native';
import { WebView } from 'react-native';

const webViewData = {
  helpDesk : {
    uri : 'http://test.kelltontech.net/nasscom_event/onlinehelp'
  },
  venueMap : {
    html: '<Image width=350 src="http://test.kelltontech.net/nasscom_event/sites/default/files/LocationMap/%5EE127FB74EAAA4A458BA17E48F6F245A78E8D3ED3FBAABBB755%5Epimgpsh_fullsize_distr.jpg"/>'
  },
  aboutKellton :{
    uri :'http://test.kelltontech.net/nasscom_event/about-kellton/34496'
  },
  aboutApp : {
    uri : 'http://test.kelltontech.net/nasscom_event/about-conf/34496'
  },
  heatMap : {
    uri : 'http://test.kelltontech.net/nasscom_event/analytic/34496'
  }
}

export default class About extends Component {

  static navigationOptions = () => ({
    title: 'Help',
    headerTintColor: '#fff',
    headerStyle: {
      backgroundColor: '#0f234f',
    },
  });

  render() {
    return (
      <View style={{flex:1,backgroundColor:'#fff'}}>
        <WebView
          source={webViewData.venueMap}
          originWhitelist={['*']}
          style={{margin:"2%",flex:1}}
        />
      </View>
    );
  }
}
