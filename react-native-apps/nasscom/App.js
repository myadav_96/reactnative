import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import Program from './src/components/program';
import { createStackNavigator } from 'react-navigation';
import MyNotes from './src/components/myNotes';
import About from './src/components/reusableComponents/about';
import LinkedInLogin from './src/components/linkedInLogin';

export default class App extends Component {
  render() {
    return (
      <RootStack  />
    );
  }
}

const RootStack = createStackNavigator({
  // program: {
  //   screen: Program
  // },
  myNotes:{
    screen: MyNotes
  },
  // about:{
  //   screen: About
  // },
  linkedInLogin:{
    screen:LinkedInLogin
  }
});