/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button,Image} from 'react-native';
import LoginName from './Login.js'


export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      email :'',
      password :'',
    };
  }
  
  updateState = (email,password) => {
      this.setState({
          email: email,
          password: password,
      });
  }

  render() {
    return (
      <View style={styles.container}>
      <Image style={styles.reactNativeLogo}
      source={require('./reactLogo.png')} />
      <LoginName style={{flex:2}} updateState={this.updateState}></LoginName>
      <View style={{flex:2}}>
        <Text style={styles.text}>{this.state.email}{"\n"}{"\n"}{this.state.password}</Text>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d0e5a7',
  },
  reactNativeLogo: {
    flex:1,
    alignSelf:"center",
    width: 220,
    height: 100,
    margin: 30,
  },
  text: {
    color:'#175170',
    fontSize:25,
    fontWeight: 'bold',
    margin: 30,
    alignSelf:"center"
  }
});
