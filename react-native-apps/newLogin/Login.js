import React, {Component} from 'react';
import {Platform,Keyboard,TouchableOpacity,TextInput, StyleSheet, Text, View,Image} from 'react-native';

export default class Login extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      email: '',
      valEmail: '',
      password: '',
      valPassword: '',
      invalidPassword:'',
    };
  }
  
  submit=()=>{
    const regExEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    const regExPass = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/
    if (regExEmail.test(this.state.email) && regExPass.test(this.state.password))
    { 
      this.setState({valEmail : this.state.email,valPassword : this.state.password});
      this.props.updateState(this.state.email,this.state.password);
      Keyboard.dismiss()
      // alert("Welcome "+ this.state.email);
    }
    else{
      alert("invalid email or password")
    }
  }

  invalidPassword=()=>{
    const regExPass = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/
    if(regExPass.test(this.state.password)){
      this.setState({invalidPassword : ''});
    }else{
      this.setState({invalidPassword : 'Invalid Password'});
      // alert("invalid password");
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput style={{backgroundColor:'#fff',margin:20}} 
            inputValue={this.state.email} 
            returnKeyType='next'
            onSubmitEditing={()=>this.passwordInput.focus()}
            onChangeText={(email) => this.setState({ email })} 
            placeholder='Enter Email' />
        <TextInput style={{backgroundColor:'#fff',margin:20}} 
            inputValue={this.state.password} 
            returnKeyType='go'
            ref={(input)=>this.passwordInput = input}
            onChangeText={(password) => {
              this.setState({ password })
              this.invalidPassword()
          }}
            // onChangeText = {this.invalidPassword} 
            maxLength={20}
            secureTextEntry={true} 
            placeholder='Enter Password' />
        <Text style={{color:'#fff'}}>{this.state.invalidPassword}</Text>
        <TouchableOpacity style={{margin:20,marginBottom:0,alignSelf:"center"}} 
            onPress={this.submit}>
                <Image style={{height:50,width:150}} 
                    source={require('./loginButton.png')} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#324c02',
    margin:20,
    borderRadius:30,
    padding:20,
    paddingBottom:10,
  },
  
});
