/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Alert,ScrollView,TextInput,Button, Platform, StyleSheet, Text, View} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    
    this.state = {
      email: '',
      valEmail: '',
    };
  }
  
  submit=()=>{
    const regEx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    if (regEx.test(this.state.email))
    { 
      this.setState({valEmail : this.state.email});
      alert("Welcome "+ this.state.email);

    }else{
      alert("You have entered an invalid email address!")
    }
  }

  render() {
    return (
      <View>
        <ScrollView style={{padding: 20}}>
                <Text 
                    style={{fontSize: 27}}>
                    Login
                </Text>
                <TextInput inputValue={this.state.email} onChangeText={(email) => this.setState({ email })} placeholder='Enter your email' />
                <View style={{margin:7}} />
                <Button onPress={this.submit} title="Submit"/>
                  <Text style={{margin:20,textAlign:"center"}}>{this.state.valEmail}</Text>
                </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
});
